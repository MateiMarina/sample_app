module ApplicationHelper

  def full_title(page_title='')                               #method definition and optional argument
    base_title = "Ruby on Rails Tutorial Sample App"          #variable assignment
    if page_title.empty?                                      #Boolean test 
      base_title                                              #Implicit return
    else
      page_title + " | " + base_title                         #String concatenation
    end
  end
end
